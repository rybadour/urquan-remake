package urquan.main_menu;

import titanium_reindeer.Enums;
import titanium_reindeer.spatial.Vector2;
import titanium_reindeer.core.InputState;
import titanium_reindeer.rendering.RenderingScene;
import titanium_reindeer.rendering.Canvas2D;

import titanium_reindeer.rendering.Color;
import titanium_reindeer.spatial.Rect;

import nape.geom.Vec2;
import nape.space.Space;
import nape.phys.Body;
import nape.phys.BodyType;
import nape.shape.Polygon;

class MainMenuScene extends RenderingScene
{
	public var game:UrquanGame;

	public var view:MainMenuView;
	public var input:InputState;

	public function new(game:UrquanGame, width:Int, height:Int, input:InputState)
	{
		super(width, height);

		this.game = game;

		this.view = new MainMenuView(width, height);
		this.input = input;
	}

	public override function update(msTimeStep:Int):Void
	{
		super.update(msTimeStep);

		if (this.input.isKeyDown(Key.Enter))
		{
			this.game.startBattle();
		}

		// TODO: Menu selection
	}

	private override function _render(canvas:Canvas2D):Void
	{
		this.view.render(canvas);
	}

	public override function resize(width:Int, height:Int):Void
	{
		super.resize(width, height);

		this.view.resize(width, height);
	}
}
