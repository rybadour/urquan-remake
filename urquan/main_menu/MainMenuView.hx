package urquan.main_menu;

import titanium_reindeer.Enums;
import titanium_reindeer.rendering.Canvas2D;
import titanium_reindeer.rendering.Color;
import titanium_reindeer.rendering.TextState;
import titanium_reindeer.spatial.Vector2;
import titanium_reindeer.ui.UIAlignment;
import titanium_reindeer.ui.UIGroup;
import titanium_reindeer.ui.UIText;

class MainMenuView extends UIGroup
{
	private var starsBg:StarsView;

	private var menu:UIGroup;

	public function new(width:Int, height:Int)
	{
		super();

		this.width = width;
		this.height = height;

		this.starsBg = new StarsView(this.width, this.height, [1, 2, 3], 300, [0.1, 0.15, 0.2]);

		var title = new UIText("Star Control Remake 64");
		title.alignment.xAlign = UIAlignX.Center;
		title.position.y += 50;
		title.text.state.fontFamily = 'Nova Flat';
		title.text.state.fontSize = 50;
		title.text.state.fillColor = Color.White;
		title.text.state.fontWeight = FontWeight.Bold;
		this.addChild(title, new UIAlignment(UIAlignX.Center, UIAlignY.Top));

		this.menu = new UIGroup();
		this.menu.position.y += 200;
		this.menu.width = 240;
		this.menu.alignment.xAlign = UIAlignX.Center;
		// TODO: Should be relative to the title probably
		this.addChild(menu, new UIAlignment(UIAlignX.Center, UIAlignY.Top));

		// TODO: These three should be put into a UIList instance or something
		var hotSeatButton = new UIText("Local - Two Players");
		hotSeatButton.alignment.xAlign = UIAlignX.Center;
		hotSeatButton.text.state.fontFamily = 'Nova Flat';
		hotSeatButton.text.state.fontSize = 30;
		hotSeatButton.text.state.fillColor = Color.White;
		hotSeatButton.text.state.fontWeight = FontWeight.Bold;
		hotSeatButton.text.state.lineWidth = 0;
		this.menu.addChild(hotSeatButton, new UIAlignment(UIAlignX.Center, UIAlignY.Top));

		var onlineButton = new UIText("Online/Multiplayer");
		onlineButton.position.y = 80;
		onlineButton.alignment.xAlign = UIAlignX.Center;
		onlineButton.text.state.fontFamily = 'Nova Flat';
		onlineButton.text.state.fontSize = 30;
		onlineButton.text.state.fillColor = Color.White;
		onlineButton.text.state.fontWeight = FontWeight.Bold;
		onlineButton.text.state.lineWidth = 0;
		this.menu.addChild(onlineButton, new UIAlignment(UIAlignX.Center, UIAlignY.Top));

		var editorButton = new UIText("Level Editor");
		editorButton.position.y = 160;
		editorButton.alignment.xAlign = UIAlignX.Center;
		editorButton.text.state.fontFamily = 'Nova Flat';
		editorButton.text.state.fontSize = 30;
		editorButton.text.state.fillColor = Color.White;
		editorButton.text.state.fontWeight = FontWeight.Bold;
		editorButton.text.state.lineWidth = 0;
		this.menu.addChild(editorButton, new UIAlignment(UIAlignX.Center, UIAlignY.Top));

		var copyright = new UIText("© Ryan Badour, 2015");
		copyright.alignment.xAlign = UIAlignX.Center;
		copyright.alignment.yAlign = UIAlignY.Bottom;
		copyright.position.y -= 30;
		copyright.text.state.fontFamily = 'Nova Flat';
		copyright.text.state.fontSize = 20;
		copyright.text.state.fillColor = Color.White;
		copyright.text.state.lineWidth = 0;
		this.addChild(copyright, new UIAlignment(UIAlignX.Center, UIAlignY.Bottom));
	}

	private override function _render(canvas:Canvas2D):Void
	{
		canvas.save();
		this.starsBg.layers.offset.addTo(new Vector2(0, 4));
		this.starsBg.render(canvas);
		canvas.restore();

		super._render(canvas);
	}

	public override function resize(width:Int, height:Int):Void
	{
		super.resize(width, height);
		this.starsBg.resize(width, height);
	}
}
