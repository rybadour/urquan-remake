package urquan;

import titanium_reindeer.core.Game;
import titanium_reindeer.core.Game;
import titanium_reindeer.spatial.RectRegion;
import titanium_reindeer.spatial.Vector2;
import titanium_reindeer.rendering.RectRenderer;
import titanium_reindeer.rendering.Canvas2D;
import titanium_reindeer.rendering.Color;
import titanium_reindeer.rendering.RenderingScene;

import urquan.main_menu.MainMenuScene;
import urquan.battle.BattleScene;

class UrquanGame extends Game
{
	public static inline var GAME_WIDTH		= 2000;
	public static inline var GAME_HEIGHT 	= 2000;

	public static inline var PATH:String	= "";

	public static inline var IN_DEBUG:Bool 	= true;

	public var activeScene:RenderingScene;

	public function new()
	{
		super("game", js.Browser.window.innerWidth, js.Browser.window.innerHeight, true);

		var fsButton = js.Browser.document.getElementById("fsButton");
		fsButton.onclick = function (e:Dynamic):Void {
			this.requestFullScreen();
		};

		js.Browser.window.onresize = function (event) { this.recalculateViewPort(); };

		this.changeScene(new MainMenuScene(this, this.width, this.height, this.input));
	}

	// TODO: Add more information to dictate what kind of battle (network or not) this is
	public function startBattle()
	{
		this.changeScene(new BattleScene(this.width, this.height, this.input)); 
	}

	private override function update(msTimeStep:Int)
	{
		super.update(msTimeStep);

		// Clear the screen before the scene renders
		this.activeScene.update(msTimeStep);

		this.pageCanvas.clear();
		this.activeScene.render(this.pageCanvas);
	}

	private function changeScene(newScene:RenderingScene)
	{
		if (this.activeScene != null)
			this.activeScene.pause();

		this.activeScene = newScene;
		this.activeScene.unpause();
	}

	public override function resize(width:Int, height:Int):Void
	{
		super.resize(width, height);

		this.activeScene.resize(width, height);
	}
}
