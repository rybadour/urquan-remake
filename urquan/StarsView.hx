package urquan;

import titanium_reindeer.spatial.Vector2;
import titanium_reindeer.spatial.Rect;
import titanium_reindeer.rendering.Canvas2D;
import titanium_reindeer.rendering.IRenderer;
import titanium_reindeer.rendering.RenderState;
import titanium_reindeer.rendering.ParallaxRendererList;
import titanium_reindeer.rendering.RepeatFillRenderer;
import titanium_reindeer.rendering.Color;

class StarsView implements IRenderer
{
	private var canvases:Array<Canvas2D>;
	public var layers:ParallaxRendererList<RenderState, RepeatFillRenderer>;

	public var width(default, null):Int;
	public var height(default, null):Int;

	public function new(width:Int, height:Int, sizes:Array<Int>, count:Int, parallaxes:Array<Float>)
	{
		this.canvases = new Array();
		this.layers = new ParallaxRendererList(new RenderState(), function (i:Int, renderer:RepeatFillRenderer, offset:Vector2, canvas:Canvas2D) {
			renderer.offset = offset;
		});

		for (i in 0...parallaxes.length)
		{
			var canvas = new Canvas2D("stars", width, height);

			var repeater = new RepeatFillRenderer(canvas, width, height, RepeatFillMethod.Both, width, height);
			this.canvases[i] = canvas;
			this.layers.addLayer(repeater, parallaxes[i]);
		}

		for (s in 0...count)
		{
			var si = Std.random(sizes.length);
			var li = Std.random(this.layers.numLayers);
			var canvas = this.canvases[li];

			canvas.ctx.save();
			var rad = sizes[si];
			var x = Math.random() * width;
			var y = Math.random() * height;
			var grad = canvas.ctx.createRadialGradient(x, y, 0, x, y, rad);
			grad.addColorStop(0, Color.White.rgba);
			grad.addColorStop(1, Color.Black.rgba);
			canvas.ctx.fillStyle = grad;
			canvas.ctx.beginPath();
			canvas.ctx.arc(x, y, rad, 0, Math.PI*2, false);
			canvas.ctx.fill();
			canvas.ctx.restore();
		}

		this.width = width;
		this.height = height;
	}

	public function render(canvas:Canvas2D)
	{
		canvas.clear();
		canvas.ctx.save();
		canvas.ctx.fillStyle = Color.Black.rgba;
		canvas.renderRect(new Rect(this.width, this.height));
		canvas.ctx.restore();

		this.layers.render(canvas);
	}
	
	public function resize(width:Int, height:Int):Void
	{
		for (layer in this.layers.layers)
		{
			// Calling resize would make the sourceWidth inconsistent with the stars canvases
			layer.fillWidth = width;
			layer.fillHeight = height;
		}

		this.width = width;
		this.height = height;
	}
}
