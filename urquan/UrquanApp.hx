import urquan.UrquanGame;

class UrquanApp
{
	public static function main():Void
	{
		var game:UrquanGame = new UrquanGame();
		game.play();
	}
}
