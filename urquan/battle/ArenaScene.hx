package urquan.battle;

import titanium_reindeer.assets.ImageLoader;
import titanium_reindeer.core.InputState;
import titanium_reindeer.nape.NapeSpace;
import titanium_reindeer.rendering.Canvas2D;
import titanium_reindeer.rendering.RenderingScene;
import titanium_reindeer.spatial.Vector2;
import titanium_reindeer.util.Utility;
import titanium_reindeer.Enums;

// When two players have chosen ships
// and they actively fight in space
class ArenaScene extends RenderingScene
{
	public static inline var MAX_SPEED_SEC:Float = 200;


	public var view(default, null):ArenaView;

	public var shipA(default, null):Ship;
	public var shipB(default, null):Ship;
	public var asteroids(default, null):Array<Asteroid>;

	public var controllerA(default, null):InputShipController;
	public var controllerB(default, null):InputShipController;

	private var nSpace:NapeSpace;

	public function new(width:Int, height:Int, input:InputState, images:ImageLoader)
	{
		super(width, height);

		this.view = new ArenaView(this);

		this.shipA = new Ship(new Vector2(100, 0));
		this.shipA.view = new ShipView(this.shipA, images.get("fighter.png"));

		this.shipB = new Ship(new Vector2(-100, 0));
		this.shipB.view = new ShipView(this.shipB, images.get("fighter.png"));

		this.controllerA = new InputShipController(input, this.shipA, Key.UpArrow, Key.DownArrow, Key.LeftArrow, Key.RightArrow);
		this.controllerB = new InputShipController(input, this.shipB, Key.W, Key.S, Key.A, Key.D);

		this.nSpace = new NapeSpace(new Vector2(0, 0));

		this.nSpace.addBody(this.shipA);
		this.nSpace.addBody(this.shipB);

		this.asteroids = new Array();
		this.generateAsteroids(10, 300, 300);
	}

	public function generateAsteroids(n:Int, width:Int, height:Int):Void
	{
		var maxVelo = 100;
		for (i in 0...n)
		{
			var asteroid = new Asteroid(new Vector2(Utility.randomWithNeg(width), Utility.randomWithNeg(height)));
			asteroid.velocity = new Vector2(Utility.randomWithNeg(maxVelo), Utility.randomWithNeg(maxVelo));
			this.asteroids.push(asteroid);
			this.nSpace.addBody(asteroid);
		}
	}

	private override function _update(msTimeStep:Int):Void
	{
		this.controllerA.update(msTimeStep);
		this.controllerB.update(msTimeStep);

		// Ensure neither ship exceeds max speed
		if (this.shipA.velocity.getMagnitude() > MAX_SPEED_SEC)
		{
		}

		if (this.shipB.velocity.getMagnitude() > MAX_SPEED_SEC)
		{
		}

		this.nSpace.update(msTimeStep);
	}

	private override function _render(canvas:Canvas2D):Void
	{
		super._render(canvas);

		this.view.render(canvas);
	}

	public override function resize(width:Int, height:Int):Void
	{
		super.resize(width, height);

		//this.view.resize(width, height);
	}
}
