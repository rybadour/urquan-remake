package urquan.battle;

import js.html.Image;
import titanium_reindeer.rendering.IRenderer;
import titanium_reindeer.rendering.Canvas2D;

class ShipView implements IRenderer
{
	public var ship:Ship;
	public var image:Image;

	public function new(ship:Ship, image:Image)
	{
		this.ship = ship;
		this.image = image;
	}

	public function render(canvas:Canvas2D):Void
	{
		canvas.save();
		canvas.ctx.rotate(this.ship.direction);

		canvas.translatef(-this.image.width/2, -this.image.height/2);
		canvas.renderImage(image);
		canvas.restore();
	}
}
