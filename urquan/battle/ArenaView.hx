package urquan.battle;

import titanium_reindeer.rendering.IRenderer;
import titanium_reindeer.rendering.RenderState;
import titanium_reindeer.rendering.RendererList;
import titanium_reindeer.rendering.Color;
import titanium_reindeer.rendering.Canvas2D;
import titanium_reindeer.rendering.CanvasRenderer;
import titanium_reindeer.spatial.RectRegion;
import titanium_reindeer.spatial.Geometry;
import titanium_reindeer.spatial.Vector2;

class ArenaView implements IRenderer
{
	public static inline var MAX_ZOOM:Float = 0.5;

	public var arena(default, null):ArenaScene;
	
	public var starsView(default, null):StarsView;
	public var foreLayers(default, null):RendererList<RenderState>;

	public function new(arena:ArenaScene)
	{
		this.arena = arena;

		this.starsView = new StarsView(this.arena.width, this.arena.height, [1, 2, 3], 300, [0.75, 0.8, 0.95]);

		this.foreLayers = new RendererList(new RenderState());
	}

	public function render(canvas:Canvas2D):Void
	{
		var a = this.arena.shipA.position;
		var b = this.arena.shipB.position;

		var w = this.arena.width;
		var h = this.arena.height;

		var dist = Vector2.getDistance(a, b);
		dist *= dist;

		// Compare against the ends of the ray connecting the two ships through the center of the screen
		var diff = a.subtract(b);
		diff.normalize();
		var maxDist = Math.min(w / Math.abs(diff.x), h / Math.abs(diff.y)) / 4;
		maxDist *= maxDist*4;

		// If the ships are closer than half the ray don't bother splitting the screen
		var zoom:Float = 1;
		if (dist > maxDist)
			zoom = maxDist / dist;

		if (zoom < MAX_ZOOM)
		{
			this.splitRender(canvas, MAX_ZOOM);
		}
		else
		{
			this.cameraRender(Geometry.getMidPoint(a, b), canvas, zoom);
		}
	}

	private function splitRender(canvas:Canvas2D, zoom:Float):Void
	{
		var w = this.arena.width;
		var h = this.arena.height;
		var hw = w/2;
		var hh = h/2;
		var qw = w/4;
		var qh = h/4;

		var a = new Canvas2D("cameraA", w, h);
		var b = new Canvas2D("cameraB", w, h);

		var shipA = this.arena.shipA;
		var shipB = this.arena.shipB;

		// If true shipB is to the direct right of the split
		var center = new Vector2(hw, hh);
		var mid = Geometry.getMidPoint(shipA.position, shipB.position);
		var diff = shipA.position.subtract(shipB.position);
		diff.normalize();
		var rad = diff.getRadians();
		var splitVect = diff.getRotate(-Math.PI/2);
		var maxLength = Math.min(w / Math.abs(diff.x), h / Math.abs(diff.y)) / 4;
		
		// Find the points on the edge of the screen
		var splitEdge = this.getEdgePoint(center, splitVect, w);
		var otherSplitEdge = new Vector2(w - splitEdge.x, h - splitEdge.y);
	
		var cA = mid.subtract(shipA.position);
			cA.normalize();
			cA.extend(maxLength / zoom);
		var cB = mid.subtract(shipB.position);
			cB.normalize();
			cB.extend(maxLength / zoom);
		this.cameraCenterShip(shipA, cA, a, zoom);
		this.cameraCenterShip(shipB, cB, b, zoom);

		// Render A's Split
		canvas.save();
		canvas.translate(center);
		this.clipCamera(canvas, w, h, splitVect.getReverse());
		canvas.translate(center.getReverse());
		canvas.renderCanvas(a);
		canvas.restore();
	
		// Render B's Split
		canvas.save();
		canvas.translate(center);
		this.clipCamera(canvas, w, h, splitVect);
		canvas.translate(center.getReverse());
		canvas.renderCanvas(b);
		canvas.restore();

		canvas.save();
		canvas.ctx.beginPath();
		canvas.moveTo(splitEdge);
		canvas.lineTo(otherSplitEdge);
		canvas.ctx.lineWidth = 5;
		canvas.ctx.strokeStyle = Color.White.rgba;
		canvas.ctx.stroke();
		canvas.restore();
	}

	private function cameraCenterShip(ship:Ship, cameraOffset:Vector2, canvas:Canvas2D, zoom:Float):Void
	{
		var offset = ship.position;
		this.cameraRender(cameraOffset.add(offset), canvas, zoom);
	}

	private function cameraRender(cameraOffset:Vector2, canvas:Canvas2D, zoom:Float):Void
	{
		canvas.save();

		// Center screen
		var w = this.arena.width;
		var h = this.arena.height;
		var screenOffset = new Vector2(w/2, h/2);
		cameraOffset.reverse();

		this.starsView.layers.offset = cameraOffset.add(screenOffset);
		this.starsView.render(canvas);

		canvas.translate(screenOffset);

		canvas.scale(zoom);

		canvas.translate(cameraOffset);

		this.renderShip(this.arena.shipA, Color.Red, canvas, zoom);
		this.renderShip(this.arena.shipB, Color.Blue, canvas, zoom);

		for (asteroid in this.arena.asteroids)
		{
			canvas.save();
			canvas.translate(asteroid.position);
			asteroid.view.render(canvas);
			canvas.restore();
		}

		canvas.restore();
	}

	// Borrow directly from github repo at:
	// https://github.com/evanw/rapt
	// Finds the points relative to the center of a canvas to create a clipping region for a half
	// It assumes width and height is the bounds of the region to be clipped to
	// splitVect is a vector from the center towards the edge of the region
	// Translations should be made before this to ensure correct points are chosen
	public function clipCamera(canvas:Canvas2D, width:Float, height:Float, splitVect:Vector2)
	{
		var w = width/2;
		var h = height/2;
        var tx = h / splitVect.y;
        var ty = w / splitVect.x;
        canvas.ctx.beginPath();
        if ((-w) * splitVect.y - (-h) * splitVect.x >= 0)
			canvas.ctx.lineTo(-w, -h);
        if (Math.abs(splitVect.y * ty) <= h)
		   	canvas.ctx.lineTo(-splitVect.x * ty, -splitVect.y * ty);
        if ((-w) * splitVect.y - (h) * splitVect.x >= 0)
			canvas.ctx.lineTo(-w, h);
        if (Math.abs(splitVect.x * tx) <= w)
			canvas.ctx.lineTo(splitVect.x * tx, splitVect.y * tx);
        if ((w) * splitVect.y - (h) * splitVect.x >= 0)
			canvas.ctx.lineTo(w, h);
        if (Math.abs(splitVect.y * ty) <= h)
			canvas.ctx.lineTo(splitVect.x * ty, splitVect.y * ty);
        if ((w) * splitVect.y - (-h) * splitVect.x >= 0)
			canvas.ctx.lineTo(w, -h);
        if (Math.abs(splitVect.x * tx) <= w)
			canvas.ctx.lineTo(-splitVect.x * tx, -splitVect.y * tx);
        canvas.ctx.closePath();
        canvas.ctx.clip();
	}

	public function renderShip(ship:Ship, color:Color, canvas:Canvas2D, zoom:Float)
	{
		canvas.save();
		canvas.translate(ship.position);
		ship.view.render(canvas);
		canvas.restore();
	}

	// Finds a point on the edge of a rectangle
	// Finds the first point from the center in the direction specified 
	// (x, y) for (dir.x, dir.y) becomes (-x, -y) for (-dir.x, -dir.y)
	//
	// mid is the center of the rectangle
	// dir is the direction from the center
	// width is the width of the rectangle
	private function getEdgePoint(mid:Vector2, dir:Vector2, width:Float):Vector2
	{
		// Find perpendicular edge points
		var rat = dir.y/dir.x;
		var x = mid.x - 1/rat * mid.y;
		var y = 0.0;
		// x is outside the bounds
		if (x < 0 || x > width)
		{
			// a left or right edge must be hit first
			x = 0;
			y = mid.y - rat * mid.x;
		}

		return new Vector2(x, y);
	}

	public function resize(width:Int, height:Int):Void
	{
		this.starsView.resize(width, height);
	}
}
