package urquan.battle;

import titanium_reindeer.Enums;
import titanium_reindeer.core.InputState;

class InputShipController
{
	public var input(default, null):InputState;
	public var ship(default, null):Ship;

	public var thrustKey(default, null):Key;
	public var brakeKey(default, null):Key;
	public var leftKey(default, null):Key;
	public var rightKey(default, null):Key;

	public function new(input:InputState, ship:Ship, thrustKey:Key, brakeKey:Key, leftKey:Key, rightKey:Key)
	{
		this.input = input;
		this.ship = ship;

		this.thrustKey = thrustKey;
		this.brakeKey = brakeKey;
		this.leftKey = leftKey;
		this.rightKey = rightKey;
	}

	public function update(msTimeStep:Int):Void
	{
		if (this.input.isKeyDown(this.thrustKey))
			this.ship.applyThrust(msTimeStep);

		if (this.input.isKeyDown(this.leftKey))
			this.ship.rotateLeft(msTimeStep);

		if (this.input.isKeyDown(this.rightKey))
			this.ship.rotateRight(msTimeStep);
	}
}
