package urquan.battle;

import titanium_reindeer.spatial.Vector2;
import titanium_reindeer.nape.NapeDynamicBody;
import titanium_reindeer.nape.Vector2ToVec2;

class Ship extends NapeDynamicBody
{
	public static inline var THRUST_SEC:Int   = 100;
	public static var ROTATE_SEC:Float = Math.PI;


	public var view:ShipView;

	public function new(pos:Vector2)
	{
		super(pos);

		this.addCirclef(20);
	}

	public function rotateLeft(msTimeStep:Int):Void
	{
		this.stopRotating();
		this.direction -= this.amountPerSecF(ROTATE_SEC, msTimeStep);
	}

	public function rotateRight(msTimeStep:Int):Void
	{
		this.stopRotating();
		this.direction += this.amountPerSecF(ROTATE_SEC, msTimeStep);
	}

	public function applyThrust(msTimeStep:Int):Void
	{
		var thrust = new Vector2(this.amountPerSec(THRUST_SEC, msTimeStep), 0);
		thrust.rotate(direction);
		this.nBody.applyImpulse(Vector2ToVec2.toVec2(thrust));
	}

	// Privates
	// --------------------------------------------------------------

	private function amountPerSecF(value:Float, msTimeStep:Int):Float
	{
		return value * msTimeStep/1000;
	}
	
	private function amountPerSec(value:Int, msTimeStep:Int):Int
	{
		return Math.round(value * msTimeStep/1000);
	}
}
