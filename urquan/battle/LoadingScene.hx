package urquan.battle;

import titanium_reindeer.rendering.Color;
import titanium_reindeer.rendering.Canvas2D;
import titanium_reindeer.rendering.RenderingScene;
import titanium_reindeer.rendering.ProgressBarRenderer;
import titanium_reindeer.rendering.TextRenderer;
import titanium_reindeer.rendering.TextState;
import titanium_reindeer.assets.AssetLoader;

class LoadingScene extends RenderingScene
{
	public static inline var MARGIN = 60;
	public static inline var HEIGHT = 40;
	

	public var progressBar:ProgressBarRenderer;
	public var message:TextRenderer;

	public var assetLoader:AssetLoader;

	public function new(width:Int, height:Int, assetLoader:AssetLoader)
	{
		super(width, height);

		// Configure the look of things
		this.progressBar = new ProgressBarRenderer(assetLoader, this.width - MARGIN*2, HEIGHT);

		this.message = new TextRenderer("Loading");
		this.message.state.fillColor = Color.White;
		this.message.state.fontSize = 30;
		this.message.state.fontWeight = FontWeight.Bold;

		// Load assets
		this.assetLoader = assetLoader;
		// TODO: Load more assets
		this.assetLoader.load();
	}

	private override function _render(canvas:Canvas2D):Void
	{
		var bottom = this.height - MARGIN - HEIGHT;
		canvas.save();
			canvas.save();
				canvas.ctx.fillStyle = 'rgb(0, 0, 0)';
				canvas.ctx.fillRect(0, 0, width, height);
			canvas.restore();

			canvas.translatef(0, bottom);
			canvas.save();
				canvas.translatef(this.width - 100, -20);
				this.message.render(canvas);
				canvas.restore();

				canvas.save();
				canvas.translatef(MARGIN, 0);
				this.progressBar.render(canvas);
			canvas.restore();
		canvas.restore();
	}
}
