package urquan.battle;

import titanium_reindeer.rendering.RenderingScene;
import titanium_reindeer.rendering.Canvas2D;
import titanium_reindeer.assets.AssetLoader;
import titanium_reindeer.assets.ImageLoader;
import titanium_reindeer.assets.ImageAsset;
import titanium_reindeer.core.InputState;

// The main scene for a battle
// between two players and their fleets
class BattleScene extends RenderingScene
{
	public var loadingScene(default, null):LoadingScene;
	public var arenaScene(default, null):ArenaScene;
	public var activeScene(default, null):RenderingScene;

	public var assetLoader(default, null):AssetLoader;
	public var imageLoader(default, null):ImageLoader;
	public var input(default, null):InputState;

	public function new(width:Int, height:Int, input:InputState)
	{
		super(width, height);

		this.imageLoader = new ImageLoader([
			new ImageAsset("artillery.png"),
			new ImageAsset("bladeTailHeadOn.png"),
			new ImageAsset("bladeTailHead.png"),
			new ImageAsset("bladeTailSegmentOn.png"),
			new ImageAsset("bladeTailSegment.png"),
			new ImageAsset("fighter.png"),
			new ImageAsset("mineLayer.png"),
			new ImageAsset("mine.png"),
			new ImageAsset("ship1.png"),
			new ImageAsset("ship2.png"),
		], "assets/img/");

		this.assetLoader = new AssetLoader([
			this.imageLoader
		]);

		this.input = input;

		// Start the loading scene
		this.loadingScene = new LoadingScene(width, height, assetLoader);
		this.setActiveScene(this.loadingScene);
	}

	private function setActiveScene(scene:RenderingScene)
	{
		this.activeScene = scene;
	}

	private override function _update(msTimeStep:Int):Void
	{
		super._update(msTimeStep);

		this.activeScene.update(msTimeStep);

		// Wait for loading assets to be done
		// Then load the arena scene
		if (this.loadingScene != null && this.assetLoader.isLoaded())
		{
			this.loadingScene = null;
			this.arenaScene = new ArenaScene(this.width, this.height, this.input, this.imageLoader);
			this.setActiveScene(this.arenaScene);
		}
	}

	private override function _render(canvas:Canvas2D):Void
	{
		this.activeScene.render(canvas);
	}


	public override function resize(width:Int, height:Int):Void
	{
		super.resize(width, height);
		this.activeScene.resize(width, height);
	}
}
