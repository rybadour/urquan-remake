package urquan.battle;

import titanium_reindeer.spatial.Vector2;
import titanium_reindeer.nape.NapeDynamicBody;
import titanium_reindeer.rendering.CircleRenderer;
import titanium_reindeer.rendering.Color;
import titanium_reindeer.spatial.Circle;

class Asteroid extends NapeDynamicBody
{
	public var view:CircleRenderer;

	public function new(pos:Vector2)
	{
		super(pos);

		// Add circle for collision
		this.addCirclef(10);

		this.view = new CircleRenderer(new Circle(10));
		this.view.state.fillColor = Color.Clear;
		this.view.state.lineWidth = 2;
		this.view.state.strokeColor = Color.White;
	}
}
